if [ -z $NGINX_DIR ]; then
    NGINX_DIR=/etc/nginx
fi

if [ $ENABLE_SSL == "FALSE" ]; then
    echo "No SSL configured"
else
    trap "sv stop nginx; exit" SIGTERM
    if [ ! -f /cert/dhparam.pem ]; then
        echo "No dhparam file found."
        openssl dhparam -out /cert/dhparam.pem 2048
    fi
    if [ ! -f /cert/privkey.pem ]; then
        echo "No SSL certificate found."
        echo "Generating self-signed certificate"
        ( set -x; openssl req -x509 -nodes -days 365 -newkey rsa:2048 -subj "/CN=localhost" -keyout /cert/privkey.pem -out /cert/fullchain.pem )
    fi
fi

export NGINX_ENABLE_SSL_COMMENT=""
export NGINX_GEOWEB_READ_PERMISSION="FALSE"
export NGINX_GEOWEB_WRITE_PERMISSION="FALSE"
export NGINX_ALLOW_ANONYMOUS_ACCESS="FALSE"
export NGINX_ENABLE_ACCESS_LOG="TRUE"

if [ "$ENABLE_IPV6" == "FALSE" ]; then
    echo "No IPv6 configured"
    export NGINX_ENABLE_IPV6_COMMENT="#"
fi

if [ $ENABLE_SSL == "FALSE" ]; then 
    export NGINX_ENABLE_SSL_COMMENT="#"
fi

if [ -n "$GEOWEB_REQUIRE_READ_PERMISSION" ]; then
    NGINX_GEOWEB_READ_PERMISSION=$GEOWEB_REQUIRE_READ_PERMISSION
fi

if [ -n "$GEOWEB_REQUIRE_WRITE_PERMISSION" ]; then
    NGINX_GEOWEB_WRITE_PERMISSION=$GEOWEB_REQUIRE_WRITE_PERMISSION
fi

if [ -n "$ALLOW_ANONYMOUS_ACCESS" ]; then
    NGINX_ALLOW_ANONYMOUS_ACCESS=$ALLOW_ANONYMOUS_ACCESS
fi

if [ -n "$ENABLE_ACCESS_LOG" ]; then
    NGINX_ENABLE_ACCESS_LOG=$ENABLE_ACCESS_LOG
fi

export DNS_SERVER_IP=`cat /etc/resolv.conf |grep -i '^nameserver'|head -n1|cut -d ' ' -f2`

envsubst '\$JWKS_URI \$ENABLE_DEBUG_LOG \$NGINX_ALLOW_ANONYMOUS_ACCESS \$NGINX_GEOWEB_READ_PERMISSION \$NGINX_GEOWEB_WRITE_PERMISSION \$OAUTH2_USERINFO \$DNS_SERVER_IP \$BACKEND_HOST \$ENABLE_SSL \$NGINX_ENABLE_SSL_COMMENT \$NGINX_ENABLE_IPV6_COMMENT \$NGINX_PORT_HTTP \$NGINX_PORT_HTTPS \$NGINX_ENABLE_ACCESS_LOG' < /nginx.conf > $NGINX_DIR/nginx.conf

if [ $ENABLE_DEBUG_LOG == "TRUE" ]; then 
    exec /usr/sbin/nginx -c $NGINX_DIR/nginx.conf -g "daemon off;error_log /dev/stdout debug;"
else
    exec /usr/sbin/nginx -c $NGINX_DIR/nginx.conf -g "daemon off;"
fi