FROM node:21 as njs-build

WORKDIR /njs
COPY njs/package.json njs/package-lock.json njs/rollup.config.mjs njs/babel.config.js ./
COPY njs/src ./src
RUN npm ci && npm run build

FROM nginxinc/nginx-unprivileged:1-alpine

USER root
RUN apk update && apk add --no-cache gettext openssl nginx-mod-http-js
COPY nginx.conf /nginx.conf
COPY nginx.sh /nginx.sh
RUN chmod a+x /nginx.sh
COPY --from=njs-build oauth2.js /etc/nginx/oauth2.js
COPY auth.conf /etc/nginx/auth.conf
USER $UID
ENTRYPOINT cp /nginx.sh /tmp; . /tmp/nginx.sh
