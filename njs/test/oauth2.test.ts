/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * */

import {
  GEOWEB_ROLE_PRESETS_ADMIN,
  GEOWEB_ROLE_USER,
  checkAudience,
  checkRequiredClaims,
  groupsToRoles,
} from '../src/oauth2';
import { JWTClaims } from '../src/types';

describe('checkRequiredClaims', () => {
  const OLD_ENV = process.env;
  beforeEach(() => {
    jest.resetModules();
    process.env = { ...OLD_ENV };
  });
  afterAll(() => {
    process.env = OLD_ENV;
  });
  it('should return false and return 401 if require_claim is unset', () => {
    const r = {
      log: jest.fn(),
      return: jest.fn(),
      variables: { require_claim: undefined },
    };
    const payload: JWTClaims = {};
    const result = checkRequiredClaims(r, payload);
    expect(result).toBe(false);
    expect(r.return).toHaveBeenCalledWith(401);
  });

  it('should return true if require_claim is "FALSE"', () => {
    const r = {
      log: jest.fn(),
      return: jest.fn(),
      variables: { require_claim: 'FALSE' },
    };
    const payload: JWTClaims = {};
    const result = checkRequiredClaims(r, payload);
    expect(result).toBe(true);
    expect(r.return).not.toHaveBeenCalled();
  });

  it('should return true if the required claim is present in the payload', () => {
    const r = {
      log: jest.fn(),
      return: jest.fn(),
      variables: { require_claim: 'claimName=value1,value2' },
    };
    const payload: JWTClaims = {
      claimName: ['value1', 'value2', 'value3'],
    };
    const result = checkRequiredClaims(r, payload);
    expect(result).toBe(true);
    expect(r.return).not.toHaveBeenCalled();
  });

  // TODO This test fails because there's currently no way to differentiate between partial and complete claim matches
  // See https://gitlab.com/opengeoweb/backend-services/auth-backend/-/issues/6
  it.skip('should return false for partial claim matches if not explicitly configured', () => {
    const r = {
      log: jest.fn(),
      return: jest.fn(),
      variables: { require_claim: 'claimName=value1' },
    };
    const payload: JWTClaims = {
      claimName: 'value123',
    };
    const result = checkRequiredClaims(r, payload);
    expect(result).toBe(false);
    expect(r.return).not.toHaveBeenCalled();
  });

  it('should return true if the required nested claim is present in the payload', () => {
    const r = {
      log: jest.fn(),
      return: jest.fn(),
      variables: { require_claim: 'realm_access.roles=presets-admin' },
    };
    const payload: JWTClaims = {
      realm_access: {
        roles: [
          'offline_access',
          'default-roles-geoweb',
          'uma_authorization',
          'presets-admin',
        ],
      },
    };
    const result = checkRequiredClaims(r, payload);
    expect(result).toBe(true);
    expect(r.return).not.toHaveBeenCalled();
  });

  it('should return false if the required claim is not present in the payload', () => {
    const r = {
      log: jest.fn(),
      return: jest.fn(),
      variables: { require_claim: 'claimName=value1,value2' },
    };
    const payload: JWTClaims = {
      claimName: ['value3', 'value4'],
    };
    const result = checkRequiredClaims(r, payload);
    expect(result).toBe(false);
    expect(r.return).not.toHaveBeenCalled();
  });
});

describe('groupsToRoles', () => {
  it('should return default user role if groups is undefined', () => {
    const result = groupsToRoles(undefined);
    expect(result).toEqual([GEOWEB_ROLE_USER]);
  });

  it('should return default user role if groups is an empty array', () => {
    const result = groupsToRoles([]);
    expect(result).toEqual([GEOWEB_ROLE_USER]);
  });

  it('should return default user role and preset-admin role if required group for preset-admin matches required group', () => {
    const authConfig = {
      GEOWEB_ROLE_CLAIM_VALUE_PRESETS_ADMIN: 'presetsAdminsGroup',
    };
    const groups = ['presetsAdminsGroup', 'allUsersGroup'];
    const result = groupsToRoles(groups, authConfig);
    expect(result).toEqual([GEOWEB_ROLE_USER, GEOWEB_ROLE_PRESETS_ADMIN]);
  });

  it('should return only default user role if required group for preset-admin does not match any group', () => {
    const authConfig = {
      GEOWEB_ROLE_CLAIM_VALUE_PRESETS_ADMIN: 'presetsAdminsGroup',
    };
    const groups = ['allUsersGroup, AdminsGroup'];
    const result = groupsToRoles(groups, authConfig);
    expect(result).toEqual([GEOWEB_ROLE_USER]);
  });

  it('aud should support string', () => {
    const r = {
      log: jest.fn(),
      return: jest.fn(),
      variables: { require_claim: 'realm_access.roles=presets-admin' },
    };
    process.env.AUD_CLAIM_VALUE = 'account';
    const result = checkAudience(r, { aud: 'account' } as JWTClaims);
    expect(result).toEqual(true);
  });
  it('aud should support a list', () => {
    const r = {
      log: jest.fn(),
      return: jest.fn(),
      variables: { require_claim: 'realm_access.roles=presets-admin' },
    };
    process.env.AUD_CLAIM_VALUE = 'otherAccount';
    const result = checkAudience(r, {
      aud: ['account', 'otherAccount'],
    } as JWTClaims);
    expect(result).toEqual(true);
  });
});
