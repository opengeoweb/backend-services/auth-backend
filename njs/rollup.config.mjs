// @ts-check
import * as FS from 'node:fs';

import { babel } from '@rollup/plugin-babel';
import commonjs from '@rollup/plugin-commonjs';
import { nodeResolve } from '@rollup/plugin-node-resolve';

// Built-in NJS modules
const njsExternals = ['crypto', 'fs', 'querystring', 'xml', 'zlib'];

/**
 * @type {import('./package.json')}
 */
const pkg = JSON.parse(FS.readFileSync('./package.json', 'utf8'));

/**
 * @type {import('rollup').RollupOptions}
 */
const options = {
  input: 'src/index.ts',
  external: njsExternals,
  plugins: [
    babel({
      babelHelpers: 'bundled',
      envName: 'njs',
      extensions: ['.ts', '.mjs', '.js'],
    }),
    nodeResolve({
      extensions: ['.mjs', '.js', '.json', '.ts'],
    }),
    // Convert CommonJS modules to ES6 modules.
    commonjs(),
  ],
  output: {
    file: pkg.main,
    format: 'es',
  },
};

export default options;
