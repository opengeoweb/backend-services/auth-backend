'use strict';
// @ts-check

/** @type {babel.ConfigFunction} */
module.exports = (api) => ({
  presets: [
    ...(api.env('njs') ? ['babel-preset-njs'] : []),
    [
      '@babel/preset-typescript',
      {
        allowDeclareFields: true,
      },
    ],
  ],

  plugins: [
    ...(!api.caller((c) => c && c.supportsStaticESM)
      ? ['@babel/plugin-transform-modules-commonjs']
      : []),
  ],
});
