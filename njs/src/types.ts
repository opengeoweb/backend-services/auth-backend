// See https://www.rfc-editor.org/rfc/rfc7517#section-4
export interface JWK {
  alg?: string;
  crv?: string;
  d?: string;
  dp?: string;
  dq?: string;
  e?: string;
  ext?: boolean;
  k?: string;
  key_ops?: string[];
  kid?: string;
  kty?: string;
  n?: string;
  oth?: [
    {
      d?: string;
      r?: string;
      t?: string;
    },
  ];
  p?: string;
  q?: string;
  qi?: string;
  use?: string;
  x?: string;
  y?: string;
  x5c?: string[];
  x5t?: string;
  'x5t#S256'?: string;
  x5u?: string;
}

// See https://www.rfc-editor.org/rfc/rfc7517#section-5
export interface JWKSet {
  keys: JWK[];
}

// See https://www.rfc-editor.org/rfc/rfc7515#section-4
export interface JOSEHeader {
  kid?: string;
  alg?: string;
  crit?: string[];
  x5t?: string;
  x5c?: string[];
  x5u?: string;
  jku?: string;
  jwk?: Pick<JWK, 'kty' | 'crv' | 'x' | 'y' | 'e' | 'n'>;
  typ?: string;
  cty?: string;
}

// See https://www.rfc-editor.org/rfc/rfc7519#section-4
export interface JWTClaims {
  iss?: string;
  sub?: string;
  aud?: string | string[];
  jti?: string;
  nbf?: number;
  exp?: number;
  iat?: number;

  // Claims can contain any properties
  [propName: string]: unknown;
}

// See https://openid.net/specs/openid-connect-core-1_0.html#StandardClaims
export interface UserInfo {
  sub: string;
  name?: string;
  given_name?: string;
  family_name?: string;
  middle_name?: string;
  nickname?: string;
  preferred_username?: string;
  profile?: string;
  picture?: string;
  website?: string;
  email?: string;
  email_verified?: boolean;
  gender?: string;
  birthdate?: string;
  zoneinfo?: string;
  locale?: string;
  phone_number?: string;
  phone_number_verified?: boolean;
  updated_at?: number;
  address?: UserInfoAddress;

  // UserInfo can contain any properties
  [propName: string]: unknown;
}

// See https://openid.net/specs/openid-connect-core-1_0.html#AddressClaim
export interface UserInfoAddress {
  formatted?: string;
  street_address?: string;
  locality?: string;
  region?: string;
  postal_code?: string;
  country?: string;
}

export interface ConfigType {
  GEOWEB_ROLE_CLAIM_NAME?: string;
  GEOWEB_ROLE_CLAIM_VALUE_PRESETS_ADMIN?: string;
}
