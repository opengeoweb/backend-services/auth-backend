/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * */

import { JOSEHeader, JWKSet, JWTClaims, UserInfo, ConfigType } from './types';

/**
 * Implements the authorization logic in conjunction with NJS scripting language and auth_request nginx module
 * @param r - Nginx HTTP Request, see the documentation https://nginx.org/en/docs/njs/reference.html#http
 */
export const authorize = (r: NginxHTTPRequest) => {
  const userinfoSubrequest = (r: NginxHTTPRequest) => {
    r.subrequest('/_auth_send_userinfo_request', (reply) => {
      if (reply === undefined || reply.responseBuffer === undefined) {
        r.log(
          `[userinfoSubrequest] Access denied, unexpected response from the Userinfo endpoint`,
        );
        r.return(401);
      } else if (reply.status == 200) {
        r.log(`[userinfoSubrequest] Userinfo endpoint status 200`);
        try {
          const response: UserInfo = JSON.parse(
            reply.responseBuffer.toString(),
          );
          const requiredUsernameClaim =
            process.env.GEOWEB_USERNAME_CLAIM || 'email';
          r.log(
            `[userinfoSubrequest] Required username claim "${requiredUsernameClaim}"`,
          );
          if (!response[requiredUsernameClaim]) {
            r.log(`[userinfoSubrequest] Access denied, user was not found`);
            r.return(403);
            return;
          }
          const geowebUser = String(response[requiredUsernameClaim]);
          r.log(`[userinfoSubrequest] GeoWeb user "${geowebUser}"`);
          const roleConfig: ConfigType = {
            GEOWEB_ROLE_CLAIM_NAME: process.env.GEOWEB_ROLE_CLAIM_NAME,
            GEOWEB_ROLE_CLAIM_VALUE_PRESETS_ADMIN:
              process.env.GEOWEB_ROLE_CLAIM_VALUE_PRESETS_ADMIN,
          };
          r.log(
            `[userinfoSubrequest] Claim used to grant roles: ${roleConfig.GEOWEB_ROLE_CLAIM_NAME}`,
          );
          const groups = response[roleConfig.GEOWEB_ROLE_CLAIM_NAME!] as
            | string[]
            | undefined;
          r.log(`[userinfoSubrequest] Role claim values to check: ${groups}`);
          const roles = groupsToRoles(groups, roleConfig).join(', ');
          r.log(`[userinfoSubrequest] Roles: ${roles}`);
          r.headersOut['geowebroles'] = roles;
          const requireClaim = r.variables.require_claim;
          if (!requireClaim || requireClaim === 'FALSE') {
            r.log(`[userinfoSubrequest] No additional claims required`);
            r.headersOut['geowebusername'] = geowebUser;
            r.status = 204;
            r.sendHeader();
            r.finish();
          } else {
            const requiredClaimName = requireClaim.split('=')[0];
            const requiredClaimValues = requireClaim.split('=')[1].split(',');
            r.log(
              `[userinfoSubrequest] Requested resource requires a valid "${requiredClaimName}" claim`,
            );
            r.log(
              `[userinfoSubrequest] Valid "${requiredClaimName}" claim values includes "${requiredClaimValues}"`,
            );
            const reducedClaims = requiredClaimName
              .split('.')
              .reduce((prev: unknown, cur: string) => {
                return isObject(prev) ? prev[cur] : undefined;
              }, response);
            const hasRequiredClaim = requiredClaimValues.some((value) => {
              return (
                isStringOrStringArray(reducedClaims) &&
                reducedClaims.includes(value)
              );
            });
            if (hasRequiredClaim) {
              r.headersOut['geowebusername'] = geowebUser;
              r.status = 204;
              r.sendHeader();
              r.finish();
            } else {
              r.log(
                `[userinfoSubrequest] Access denied, the required claim value was not found`,
              );
              r.return(403);
              return;
            }
          }
        } catch (e) {
          r.log(`[userinfoSubrequest] Access denied, an exception occurred`);
          r.log(toErrorMessage(e));
          r.return(401);
        }
      } else {
        r.log(
          `[userinfoSubrequest] Access denied, Userinfo endpoint status NOT 200`,
        );
        r.return(401);
      }
    });
  };

  const jwksSubrequest = (r: NginxHTTPRequest) => {
    r.subrequest('/_auth_send_jwks_request', async (reply) => {
      if (reply === undefined || reply.responseBuffer === undefined) {
        r.log(
          `[jwksSubrequest] Access denied, unexpected response from the JWKS endpoint`,
        );
        r.return(401);
      } else if (reply.status == 200 && r.headersIn['Authorization']) {
        try {
          const requestToken = r.headersIn['Authorization'].split(' ')[1];
          const encodedSignedData =
            requestToken.split('.')[0] + '.' + requestToken.split('.')[1];
          const encodedSignature = requestToken.split('.')[2];
          const decodedHeader = Buffer.from(
            requestToken.split('.')[0],
            'base64url',
          ).toString();
          const decodedPayload = Buffer.from(
            requestToken.split('.')[1],
            'base64url',
          ).toString();
          const headerObj: JOSEHeader = JSON.parse(decodedHeader);
          const jwtClaims: JWTClaims = JSON.parse(decodedPayload);

          const hasValidSignature = await verify(
            r,
            reply.responseBuffer,
            encodedSignature,
            encodedSignedData,
            headerObj,
          );
          const hasCorrectAudience = checkAudience(r, jwtClaims);
          const hasValidIssuer = checkIssuer(r, jwtClaims);
          const hasNotExpired = checkIfExpired(r, jwtClaims);
          const hasRequiredClaim = checkRequiredClaims(r, jwtClaims);

          if (
            hasValidSignature &&
            hasCorrectAudience &&
            hasValidIssuer &&
            hasNotExpired &&
            hasRequiredClaim
          ) {
            const geowebUsernameClaim =
              process.env.GEOWEB_USERNAME_CLAIM || 'email';
            if (!jwtClaims[geowebUsernameClaim]) {
              r.log(`[jwksSubrequest] Unable to get username claim from token`);
              r.return(401);
              return;
            }
            const geowebUser = String(jwtClaims[geowebUsernameClaim]);
            r.log(`[jwksSubrequest] GeoWeb user "${geowebUser}"`);
            r.headersOut['geowebusername'] = geowebUser;

            const roleConfig: ConfigType = {
              GEOWEB_ROLE_CLAIM_NAME: process.env.GEOWEB_ROLE_CLAIM_NAME,
              GEOWEB_ROLE_CLAIM_VALUE_PRESETS_ADMIN:
                process.env.GEOWEB_ROLE_CLAIM_VALUE_PRESETS_ADMIN,
            };
            r.log(
              `[jwksSubrequest] Check roles from the "${roleConfig.GEOWEB_ROLE_CLAIM_NAME}" claim`,
            );
            const groups = jwtClaims[roleConfig.GEOWEB_ROLE_CLAIM_NAME!] as
              | string[]
              | undefined;
            r.log(`[jwksSubrequest] Available groups: "${groups}"`);
            const roles = groupsToRoles(groups, roleConfig).join(', ');
            r.log(`[jwksSubrequest] Roles: ${roles}`);
            r.headersOut['geowebroles'] = roles;

            r.status = 204;
            r.sendHeader();
            r.finish();
          } else {
            r.log(`[jwksSubrequest] Token is invalid`);
            r.return(403);
          }
        } catch (e) {
          r.log(`[jwksSubrequest] Validating JWT failed`);
          r.log(toErrorMessage(e));
          r.return(401);
        }
      } else {
        r.log(`[jwksSubrequest] Access denied, JWKS endpoint status NOT 200`);
        r.return(401);
      }
    });
  };

  delete r.headersOut['geowebusername'];
  delete r.headersOut['geowebrole'];

  if (!r.headersIn['Authorization']) {
    if (process.env.ALLOW_ANONYMOUS_ACCESS === 'TRUE') {
      r.log('[authorize] Authorization not required, access granted');
      r.return(204);
      return;
    } else {
      r.log('[authorize] Access denied, no access token in request header');
      r.return(401);
      return;
    }
  }

  const authHeader = r.headersIn['Authorization'].split(' ');
  const tokenType = authHeader[1].split('.').length === 3 ? 'jwt' : 'opaque';
  r.log(`[authorize] Token type: ${tokenType}`);

  if (
    process.env.JWKS_URI &&
    process.env.AUD_CLAIM_VALUE &&
    tokenType === 'jwt'
  ) {
    r.log(`[authorize] Validate the token using JSON Web Key Set`);
    jwksSubrequest(r);
  } else if (process.env.OAUTH2_USERINFO) {
    r.log(`[authorize] Validate the token using the Userinfo endpoint`);
    userinfoSubrequest(r);
  } else {
    r.log(`[authorize] No userinfo, JWKS URI or audience defined`);
    r.return(401);
  }
};

const getCryptoKeyObj = async (
  r: NginxHTTPRequest,
  jwkSet: JWKSet,
  kid: string,
): Promise<CryptoKey | null> => {
  try {
    const jwk = jwkSet.keys.find((key) => key.kid === kid);
    return await crypto.subtle.importKey(
      'jwk',
      jwk as KeyData,
      { name: 'RSASSA-PKCS1-v1_5', hash: 'SHA-256' },
      true,
      ['verify'],
    );
  } catch (e) {
    r.log(`[getCryptoKeyObj] Failed to get public key`);
    r.log(toErrorMessage(e));
    r.return(401);
    return null;
  }
};

const jwtVerify = async (
  r: NginxHTTPRequest,
  algorithm: SignOrVerifyAlgorithm,
  cryptoKey: CryptoKey,
  signature: string,
  data: string,
) => {
  try {
    return await crypto.subtle.verify(
      algorithm,
      cryptoKey,
      new Uint8Array(Buffer.from(signature, 'base64url')),
      new TextEncoder().encode(data),
    );
  } catch (e) {
    r.log(`[jwtVerify] Failed to verify signature`);
    r.log(toErrorMessage(e));
    r.return(401);
    return false;
  }
};

const verify = async (
  r: NginxHTTPRequest,
  replyResponseBuffer: Buffer,
  signature: string,
  signedData: string,
  header: JOSEHeader,
) => {
  if (!header.kid) {
    r.log(`[verify] No key ID in the header`);
    r.return(401);
    return false;
  }
  try {
    const jwkSet: JWKSet = JSON.parse(replyResponseBuffer.toString());
    const cryptoKey = await getCryptoKeyObj(r, jwkSet, header.kid);
    if (!cryptoKey) {
      r.log(`[verify] Crypto key is not available`);
      r.return(401);
      return false;
    }
    const algorithm: SignOrVerifyAlgorithm = { name: 'RSASSA-PKCS1-v1_5' };
    const isValid = await jwtVerify(
      r,
      algorithm,
      cryptoKey,
      signature,
      signedData,
    );
    r.log(`[verify] isValid: ${isValid}`);
    return isValid;
  } catch (e) {
    r.log(`[verify] JWT verification failed`);
    r.log(toErrorMessage(e));
    r.return(401);
    return false;
  }
};

export const checkAudience = (r: NginxHTTPRequest, jwtClaims: JWTClaims) => {
  try {
    const requiredAudClaimName = process.env.AUD_CLAIM || 'aud';
    const tokenAudience = jwtClaims[requiredAudClaimName] as String | String[];
    const requiredAudClaimValue = process.env.AUD_CLAIM_VALUE;
    const hasCorrectAudience = Array.isArray(tokenAudience)
      ? tokenAudience.includes(requiredAudClaimValue)
      : tokenAudience === requiredAudClaimValue;

    r.log(`[checkAudience] hasCorrectAudience: ${hasCorrectAudience}`);
    return hasCorrectAudience;
  } catch (e) {
    r.log(`[checkAudience] Check audience failed`);
    r.log(toErrorMessage(e));
    r.return(401);
    return false;
  }
};

const checkIssuer = (r: NginxHTTPRequest, jwtClaims: JWTClaims) => {
  if (process.env.ISS_CLAIM && process.env.ISS_CLAIM_VALUE) {
    try {
      const requiredIssuerClaimName = process.env.ISS_CLAIM || 'iss';
      const tokenIssuer = jwtClaims[requiredIssuerClaimName];
      const requiredIssuerClaimValue = process.env.ISS_CLAIM_VALUE;
      const hasValidIssuer = tokenIssuer === requiredIssuerClaimValue;
      r.log(`[checkIssuer] hasValidIssuer: ${hasValidIssuer}`);
      return hasValidIssuer;
    } catch (e) {
      r.log(`[checkIssuer] Check issuer failed`);
      r.log(toErrorMessage(e));
      r.return(401);
      return false;
    }
  } else {
    r.log(`[checkIssuer] No issuer check required`);
    return true;
  }
};

export const checkIfExpired = (r: NginxHTTPRequest, jwtClaims: JWTClaims) => {
  try {
    const now = Math.floor(Date.now() / 1000);
    const exp = jwtClaims.exp;
    const nbf = jwtClaims.nbf;
    if (nbf && exp) {
      const hasNotExpiredAndIsValid = exp > now && nbf < now;
      r.log(
        '[checkIfExpired] hasNotExpiredAndIsValid: ' + hasNotExpiredAndIsValid,
      );
      return hasNotExpiredAndIsValid;
    } else if (exp) {
      const hasNotExpired = exp > now;
      r.log('[checkIfExpired] hasNotExpired: ' + hasNotExpired);
      return hasNotExpired;
    } else {
      return false;
    }
  } catch (e) {
    r.log(`[checkIfExpired] Check if expired failed`);
    r.log(toErrorMessage(e));
    r.return(401);
    return false;
  }
};

export const checkRequiredClaims = (
  r: NginxHTTPRequest,
  jwtClaims: JWTClaims,
) => {
  try {
    const requireClaim = r.variables.require_claim;
    if (!requireClaim) {
      r.log(`[checkRequiredClaims] Claim unset, configuration is faulty`);
      r.return(401);
      return false;
    }
    if (requireClaim === 'FALSE') {
      r.log(`[checkRequiredClaims] No additional claims required`);
      return true;
    } else {
      const requiredClaimName = requireClaim.split('=')[0];
      const requiredClaimValues = requireClaim.split('=')[1].split(',');
      r.log(
        `[checkRequiredClaims] Requested resource requires a valid "${requiredClaimName}" claim`,
      );
      r.log(
        `[checkRequiredClaims] Valid "${requiredClaimName}" claim values includes "${requiredClaimValues}"`,
      );
      const reducedClaims = requiredClaimName
        .split('.')
        .reduce((prev: unknown, cur: string) => {
          return isObject(prev) ? prev[cur] : undefined;
        }, jwtClaims);
      const hasRequiredClaim = requiredClaimValues.some((value) => {
        return (
          isStringOrStringArray(reducedClaims) && reducedClaims.includes(value)
        );
      });
      r.log(`[checkRequiredClaims] hasRequiredClaim: ${hasRequiredClaim}`);
      return hasRequiredClaim;
    }
  } catch (e) {
    r.log(`[checkRequiredClaims] Check required claims failed`);
    r.log(toErrorMessage(e));
    r.return(401);
    return false;
  }
};

const isObject = (obj: unknown): obj is Record<string, unknown> => {
  return typeof obj === 'object' && obj !== null && !Array.isArray(obj);
};

const isStringOrStringArray = (obj: unknown): obj is string[] | string => {
  return (
    (Array.isArray(obj) && obj.every((value) => typeof value === 'string')) ||
    typeof obj === 'string'
  );
};

const toErrorMessage = (error: unknown): string => {
  if (error instanceof Error) return error.message;
  return String(error);
};

export const GEOWEB_ROLE_PRESETS_ADMIN = 'ROLE_PRESET_ADMIN';
export const GEOWEB_ROLE_USER = 'ROLE_USER';

export const groupsToRoles = (
  groups: string[] | undefined,
  roleConfig?: ConfigType,
): string[] => {
  if (!Array.isArray(groups)) {
    return [GEOWEB_ROLE_USER];
  }
  const requiredGroupForPresetsAdmin =
    roleConfig?.GEOWEB_ROLE_CLAIM_VALUE_PRESETS_ADMIN;
  const roles: string[] = groups.reduce(
    (acc: string[], group: string) => {
      if (
        requiredGroupForPresetsAdmin &&
        group === requiredGroupForPresetsAdmin
      ) {
        acc.push(GEOWEB_ROLE_PRESETS_ADMIN);
      }
      return acc;
    },
    [GEOWEB_ROLE_USER],
  );

  return roles;
};
