# GeoWeb auth backend

This repository contains shared configuration files and instructions for configuring an Nginx reverse proxy with authentication and authorization in front of a backend application. The Nginx reverse proxy is a gatekeeper in front of the backend application, it forwards client requests to the backend application and returns a response to the client.

## How it works

Nginx checks if the incoming request contains OAuth 2.0 bearer token in the `Authorization` header. If an access token is provided, it is validated. Access token validation is considered as an authentication. If an user can be identified, Nginx will set a header named `Geoweb-Username` with the identity and proceed with forwarding the request to the backend application. If no access token is provided and `ALLOW_ANONYMOUS_ACCESS` is enabled, Nginx will continue to forward the request to the backend application.

### Two different methods are available for validating the access token

1. The access token can be sent to the identity provider for validation using the userinfo endpoint. User-related identification information (claims) are obtained as a response to successful token validation. This is also the only way to validate an opaque type access token.

2. If access token type is JSON Web Token (JWT) it is also possible to perform standard JWT validation using JSON Web Key Set (JWKS) provided by the identity provider. After succesfull token validation claims are extracted from the access token. The JWT contains much more claims than the userinfo endpoint provides and claims in the JWT are usually customizable unlike the userinfo endpoint response.

## Configuration

First, choose the token validation method by configuring either the address of the userinfo endpoint with `OAUTH2_USERINFO` environment variable or by configuring the JSON Web Key Set address by configuring `JWKS_URI`. These URLs are provided as part of the identity providers discovery document.

Authentication is enabled per API endpoint in the `nginx.conf` file in the location directive by including the `auth.conf` file that contains common authentication settings for Nginx.

```
location /viewpreset {
    # To enable authentication for the endpoint include the auth configuration
    include /etc/nginx/auth.conf;
```

Authorization based on claims (requiring permissions) is enabled by using the `require_claim` variable in the Nginx location configuration in the `nginx.conf` file and referring values from the map directive `valid_claim`.

```
location /viewpreset {
    # To require permissions for the endpoint set required claim from the map directive
    set $require_claim $valid_claim;
```

Authorization can be configured on a per-endpoint or per-request method basis by checking if the user has the required claim values to access a specific API endpoint. Mapping between the request method and required claim or mapping between endpoint and required claim is configured in the `nginx.conf` file by using the map directive.

```
# Map request method and required permissions (claim)
map $request_method $valid_claim {
    OPTIONS "FALSE";
    HEAD  ${NGINX_GEOWEB_READ_PERMISSION};
    GET ${NGINX_GEOWEB_READ_PERMISSION};
    POST ${NGINX_GEOWEB_WRITE_PERMISSION};
    PUT ${NGINX_GEOWEB_WRITE_PERMISSION};
    DELETE ${NGINX_GEOWEB_WRITE_PERMISSION};
    PATCH ${NGINX_GEOWEB_WRITE_PERMISSION};
    default "FALSE";
    }
```

```
# Map endpoint and required permissions (claim)
map $request_uri $valid_claim {
    ~^/endpoint1.* ${NGINX_GEOWEB_READ_PERMISSION};
    ~^/endpoint2.* ${NGINX_GEOWEB_WRITE_PERMISSION};
    default "FALSE";
}
```

Specify the required claim name and values using environment variables `GEOWEB_REQUIRE_READ_PERMISSION` and `GEOWEB_REQUIRE_WRITE_PERMISSION`. The variables in the map directive are replaced by the values of these environment variables in the final nginx configuration.

Example of setting permissions based on the users `email` claim

```
# allow access to all users in a specific email domain
GEOWEB_REQUIRE_READ_PERMISSION="email=@example.com"
# allow access to only users with a specific email address
GEOWEB_REQUIRE_WRITE_PERMISSION="email=user1@example.com,user2@example.com"
```

Example of setting permissions based on the `groups` claim

```
# Using `groups` claim to set permissions for specific groups
GEOWEB_REQUIRE_READ_PERMISSION="groups=all-users"
GEOWEB_REQUIRE_WRITE_PERMISSION="groups=presets-admins"
```

Example of setting permissions based on the `roles` claim

```
# Using `roles` claim to set permissions for users with a specific role
GEOWEB_REQUIRE_READ_PERMISSION="roles=Sigmet.Read"
GEOWEB_REQUIRE_WRITE_PERMISSION="roles=Sigmet.Write"
```

## Using roles

Nginx can check the security groups found in the access token to grant roles and pass the roles to the backend application in a header named `Geoweb-Roles`. Enable groups checking and role granting by setting the environment variables `GEOWEB_ROLE_CLAIM_NAME` and `GEOWEB_ROLE_CLAIM_VALUE_PRESETS_ADMIN`.

Example of using `cognito:groups` claim to check if the user is member of the required group `administrators`

```
GEOWEB_ROLE_CLAIM_NAME="cognito:groups"
GEOWEB_ROLE_CLAIM_VALUE_PRESETS_ADMIN="administrators"
```

Example of using nested claims with Keycloak

```
# JWT payload
# {
#   "realm_access": {
#     "roles": [
#       "offline_access",
#       "default-roles-geoweb",
#       "uma_authorization",
#       "presets-admin"
#     ]
#   },
# }
GEOWEB_ROLE_CLAIM_NAME="realm_access.roles"
GEOWEB_ROLE_CLAIM_VALUE_PRESETS_ADMIN="presets-admin"
```

If the user does not belong to the required security group, Nginx only grants the `User` role by default.

## All configuration variables

The default settings are defined in the Nginx [configuration file](nginx.conf) file. This default configuration file should be replaced with the actual endpoint configuration and authentication settings of the backend application being protected.

| Variable                              | Value                       | Definition                                                                                                                                                                                             | Remarks                                                                                                                                                                                                                                                  |
| ------------------------------------- | --------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| ENABLE_SSL                            | TRUE/FALSE                  | By default enables NGINX SSL configuration. Generates a self-signed certificate if no SSL certificate is found (`/cert/privkey.pem`).                                                                  | Set to FALSE if deploying behind a load balancer and SSL is not needed.                                                                                                                                                                                  |
| ENABLE_DEBUG_LOG                      | TRUE/FALSE                  | Enable/disable debug logging.                                                                                                                                                                          | Set to TRUE when developing or debugging authentication code                                                                                                                                                                                             |
| ENABLE_ACCESS_LOG                     | TRUE/FALSE                  | Enable/disable access logging.                                                                                                                                                                         | Set to TRUE by default                                                                                                                                                                                              |
| OAUTH2_USERINFO                       | \<URL>                      | UserInfo endpoint URL. The userinfo endpoint response contains information about the end-user and is represented as a JSON object.                                                                     | The userinfo endpoint URL is often provided as part of the server's discovery document.                                                                                                                                                                  |
| BACKEND_HOST                          | \<URL>:<port_number>        | The host address of the backend server to which NGINX directs client requests.                                                                                                                         |                                                                                                                                                                                                                                                          |
| NGINX_PORT_HTTP                       | \<port_number>              | The HTTP port that NGINX listens on.                                                                                                                                                                   | For example 80                                                                                                                                                                                                                                           |
| NGINX_PORT_HTTPS                      | \<port_number>              | The HTTPS port that NGINX listens on.                                                                                                                                                                  | For example 443                                                                                                                                                                                                                                          |
| GEOWEB_USERNAME_CLAIM                 | \<claim>                    | Claim name used as a user identifier in the backend application.                                                                                                                                       | For example 'email', 'username' or 'preferred_username'                                                                                                                                                                                                  |
| GEOWEB_ROLE_CLAIM_NAME                | \<CLAIM_NAME>               | The name of the claim containing the security groups.                                                                                                                                                  | For example `GEOWEB_ROLE_CLAIM_NAME='groups'`                                                                                                                                                                                                            |
| GEOWEB_ROLE_CLAIM_VALUE_PRESETS_ADMIN | \<CLAIM_VALUE>              | The name of the security group required to grant the preset-admin role.                                                                                                                                | For example `GEOWEB_ROLE_CLAIM_VALUE_PRESETS_ADMIN='Preset_Admins'`                                                                                                                                                                                      |
| GEOWEB_REQUIRE_READ_PERMISSION        | \<CLAIM_NAME>=<CLAIM_VALUE> | The name of the claim and the required value to check if the user has permissions to access the desired endpoint.                                                                                      | Read permission is defined for the desired endpoints in the nginx.conf file. Currently, API endpoints can have only one read access permission defined using this variable. For example `GEOWEB_REQUIRE_READ_PERMISSION='cognito:groups=PresetsReadAll'` |
| GEOWEB_REQUIRE_WRITE_PERMISSION       | \<CLAIM_NAME>=<CLAIM_VALUE> | The name of the claim and the required value to check if the user has permissions to access the desired endpoint.                                                                                      | Write permission is defined for the desired endpoints in the nginx.conf file. Currently, API endpoints can have only one write access permission defined using this variable. For example `GEOWEB_REQUIRE_WRITE_PERMISSION='groups=opengeoweb/internal'` |
| JWKS_URI                              | \<URI>                      | A JSON Web Key Set URI that points to an identity provider's public key set in JSON Web Key Set (JWKS) format. This public key set is used to validate all JSON Web Tokens (JWT) issued by the server. | The JWKS URI is often provided as part of the server's discovery document.                                                                                                                                                                               |
| AUD_CLAIM                             | <CLAIM_NAME>                | Claim name used to get the token audience.                                                                                                                                                             | For example "aud"                                                                                                                                                                                                                                        |
| AUD_CLAIM_VALUE                       | <CLAIM_VALUE>               | Audience claims value. The audience that the token is intended for.                                                                                                                                    | For example "account"                                                                                                                                                                                                                                    |
| ISS_CLAIM                             | <CLAIM_NAME>                | Issuer claim name used to get the token issuer.                                                                                                                                                        | For example "iss"                                                                                                                                                                                                                                        |
| ISS_CLAIM_VALUE                       | <CLAIM_NAME>                | Issuer claims value. This is the issuer URL used to create and sign the token.                                                                                                                         | For example "http://localhost:8082/realms/geoweb"                                                                                                                                                                                                        |
| ALLOW_ANONYMOUS_ACCESS                | TRUE/FALSE                  | Allow/disallow anonymous access. Note that if an access token has been passed, it is checked even if anonymous access is allowed. Default is FALSE.                                                    | Set to TRUE if access without an access token is needed.                                                                                                                                                                                                 |
| NGINX_DIR                             | <FOLDER_NAME>               | Overwrites the nginx installation folder. Defaults to /etc/nginx                                                                                                                                       |
| ENABLE_IPV6                           | TRUE/FALSE                  | Lets you turn off / on IPV6 Support. Deafults to TRUE.                                                                                                                                                 |

## About the njs scripting language

Njs is a subset of the JavaScript language that allows extending nginx functionality. Njs is created in compliance with ECMAScript 5.1 (strict mode) with some ECMAScript 6 and later extensions. The `ngx_http_js_module` module is used to implement location and variable handlers in njs. This repository uses Babel and Rollup to compile TypeScript into a single `oauth2.js` njs file.

## Developing with Docker Compose

The Nginx reverse proxy with `oauth2.js` njs module can be started in a Docker container.
This will create a static version of the Nginx proxy which is not automatically refreshed after saving changes to the code. The `docker-compose-*.yml` files define the Nginx, Postgres, presets-backend, presets-backend-admin and Keycloak containers stack that can be used to develop the auth code.

### Create custom configuration

To easily change the values of environment variables, create an .env file

Run:

```
cat <<EOF > .env
# Example to only allow access to users who belong to the "internal" group of the Cognito user pool
ALLOW_ANONYMOUS_ACCESS="FALSE"
GEOWEB_REQUIRE_READ_PERMISSION="cognito:groups=internal"
GEOWEB_REQUIRE_WRITE_PERMISSION="cognito:groups=internal"
EOF
```

Add all environment variables with values as needed. See the environment variables documentation under [configuration](#configuration).

### Using GitLab as an Identity provider

Use your GitLab account to login to the GeoWeb frontend. Note that GitLab uses an opaque type token, so only userinfo endpoint based token validation is supported.

#### Starting up with GitLab auth

`docker compose --file=docker-compose-gitlab.yml up --build`

#### GeoWeb frontend URL:

http://localhost:5400/

#### Clean-up

`docker compose --file=docker-compose-gitlab.yml down --volumes`

#### GitLab identity provider settings

Manage applications that use GitLab as an OAuth provider [here](https://gitlab.com/groups/opengeoweb/-/settings/applications/211982). For more information, see the [GitLab documentation](https://docs.gitlab.com/ee/integration/oauth_provider.html).

### Using Keycloak as an identity provider

When logging in for the first time to the GeoWeb frontend, create a new user by selecting `"Register"`.

#### Starting up with Keycloak auth

`docker compose --file=docker-compose-keycloak.yml up --build`

#### GeoWeb frontend URL:

http://localhost:5400/

#### Clean-up

Note that this will also delete the locally created users and any changed settings.  
`docker compose --file=docker-compose-keycloak.yml down --volumes`

#### Keycloak Identity provider settings

To manage users and other authentication settings in the local Keycloak instance. Login to [local Keycloak Admin UI](http://localhost:8082/admin) with `KEYCLOAK_ADMIN=geoweb` and `KEYCLOAK_ADMIN_PASSWORD=geoweb`. For more information, see the [Keycloak documentation](https://www.keycloak.org/documentation).

### Using AWS Cognito as an identity provider

Create a new Cognito account in [AWS console](https://eu-north-1.console.aws.amazon.com/cognito/v2/idp/user-pools/eu-north-1_87SGZj0DO/users?region=eu-north-1) to login to the GeoWeb frontend. Use your intlgeoweb account.

#### Starting up with Cognito auth

`docker compose --file=docker-compose-cognito.yml up --build`

#### GeoWeb frontend URL:

http://localhost:5400/

#### Clean-up

`docker compose --file=docker-compose-cognito.yml down --volumes`

#### AWS Cognito identity provider settings

Manage users, applications and other settings in [AWS Management Console](https://eu-north-1.console.aws.amazon.com/cognito/v2/idp/user-pools/eu-north-1_87SGZj0DO/users?region=eu-north-1). Use your intlgeoweb account. For more information, see the [Amazon Cognito documentation](https://docs.aws.amazon.com/cognito/latest/developerguide/what-is-amazon-cognito.html).

### Using Microsoft Entra ID (Azure) as an identity provider

First, find out the Entra ID settings of your own organization. At least the environment variables below should be set. If using personal account, first configure application and all other settings in [Microsoft Azure Portal](https://portal.azure.com). Note that it is not possible to use JWKS based token validation for tokens issued to the Microsoft Graph API, only userinfo based validation is supported. Configure a custom api in Entra ID to use the JWKS based validation.

```
cat <<EOF > .env
# Example of Entra ID configuration using JWKS uri based authentication
GW_AUTH_CLIENT_ID=<your_application_id>
GW_AUTH_LOGIN_URL="https://login.microsoftonline.com/<your_tenant_id>/oauth2/v2.0/authorize?client_id={client_id}&response_type=code&scope=email+openid+profile+api://<your_tenant_id>/<your_api_id>&redirect_uri={app_url}/code&state={state}&code_challenge={code_challenge}&code_challenge_method=S256"
GW_AUTH_LOGOUT_URL="https://login.microsoftonline.com/<your_tenant_id>/oauth2/v2.0/logout?post_logout_redirect_uri={app_url}"
GW_AUTH_TOKEN_URL="https://login.microsoftonline.com/<your_tenant_id>/oauth2/v2.0/token"
JWKS_URI="https://login.microsoftonline.com/common/discovery/keys"
AUD_CLAIM_VALUE=<your_application_id>
ISS_CLAIM_VALUE="https://login.microsoftonline.com/<your_tenant_id>/v2.0"
GEOWEB_REQUIRE_READ_PERMISSION="roles=<your_role>"
GEOWEB_REQUIRE_WRITE_PERMISSION="roles=<your_role>"
EOF
```

#### Starting up with Entra ID auth

`docker compose --file=docker-compose-entraid.yml up --build`

#### GeoWeb frontend URL:

http://localhost:5400/

#### Clean-up

`docker compose --file=docker-compose-entraid.yml down --volumes`

#### Microsoft Entra ID identity provider settings

Manage users, applications and other settings in [Microsoft Azure Portal](https://portal.azure.com). Use either your personal Microsoft account or your organization's account. For more information, see the [Microsoft Entra ID documentation](https://learn.microsoft.com/en-us/entra/identity/).
